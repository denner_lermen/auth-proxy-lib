package com.getbyond.auth.proxy.domain.services.impl;

import com.getbyond.auth.proxy.domain.model.ApplicationUser;
import com.getbyond.auth.proxy.domain.objectValue.AuthorizationToken;
import com.getbyond.auth.proxy.domain.repository.ApplicationUserRepository;
import com.getbyond.auth.proxy.domain.services.ApplicationUserService;
import java.util.Optional;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class AuthenticationServiceImpl implements ApplicationUserService {

  private ApplicationUserRepository applicationUserRepository;

  @Override
  public Optional<ApplicationUser> findBy(AuthorizationToken authorizationToken) {
    return applicationUserRepository.findMe(authorizationToken);
  }

}
