package com.getbyond.auth.proxy.domain.services;

public interface CloudTokenService {

  String getToken();


  String getMasterToken();
}
