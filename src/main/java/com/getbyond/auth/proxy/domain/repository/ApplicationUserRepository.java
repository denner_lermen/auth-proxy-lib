package com.getbyond.auth.proxy.domain.repository;

import com.getbyond.auth.proxy.domain.model.ApplicationUser;
import com.getbyond.auth.proxy.domain.objectValue.AuthorizationToken;
import java.util.Optional;

public interface ApplicationUserRepository {

  Optional<ApplicationUser> findMe(AuthorizationToken authorizationToken);
}
