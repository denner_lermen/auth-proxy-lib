package com.getbyond.auth.proxy.domain.objectValue;

import java.io.Serializable;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.util.Assert;

@Getter
@Accessors(chain = true)
@EqualsAndHashCode(of = "token")
@ToString
public class AuthenticationToken implements Serializable {

  private String key;
  private String companyId;
  private String userId;

  private AuthenticationToken() {
  }

  public static AuthenticationToken of(String key, String companyId, String userId) {
    Assert.hasText(key, "key.empty");
    Assert.notNull(userId, "userId.is.empty");
    AuthenticationToken authorizationToken = new AuthenticationToken();
    authorizationToken.userId = userId;
    authorizationToken.companyId = companyId;
    authorizationToken.key = key;
    return authorizationToken;
  }

}
