package com.getbyond.auth.proxy.domain.services.impl;

import com.getbyond.auth.proxy.domain.services.CloudTokenService;
import javax.servlet.http.HttpServletRequest;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

@Service
public class CloudTokenServiceImpl implements CloudTokenService {


  @Autowired
  private HttpServletRequest req;
  @Value("${byond.services.token}")
  private String masterToken;

  @Override
  public String getToken() {
    return req.getHeader(HttpHeaders.AUTHORIZATION);
  }

  @Override
  public String getMasterToken() {
    return "Bearer " + masterToken;
  }
}
