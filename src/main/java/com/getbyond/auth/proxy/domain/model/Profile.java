package com.getbyond.auth.proxy.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

@Data
@NoArgsConstructor
public class Profile implements GrantedAuthority {

  @JsonIgnore
  public static final String PUBLIC_ROLE = "PUBLIC";

  private String id;
  private String name;
  private String role;

  public static Profile nonAuthenticated() {
    Profile profile = new Profile();
    profile.setRole(PUBLIC_ROLE);
    return profile;
  }

  @Override
  public String getAuthority() {
    return role;
  }
}
