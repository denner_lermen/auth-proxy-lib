package com.getbyond.auth.proxy.domain.repository;

import com.getbyond.auth.proxy.domain.model.Company;
import com.getbyond.auth.proxy.domain.objectValue.CompanyId;
import java.util.Optional;

public interface CompanyRepository {

  Optional<Company> findById(CompanyId companyId);

}
