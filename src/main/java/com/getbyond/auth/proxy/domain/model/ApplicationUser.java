package com.getbyond.auth.proxy.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.getbyond.auth.proxy.domain.objectValue.CompanyId;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
public class ApplicationUser implements Serializable {

  private String id;
  private String name;
  private String email;
  private CompanyId company;
  private Set<Profile> roles;


  public ApplicationUser() {
    roles = new HashSet<>();
    roles.add(Profile.nonAuthenticated());
  }

  @JsonIgnore
  public boolean isSuper() {
    return roles.stream().filter(x -> x.getAuthority().equals("SUPER")).findFirst().isPresent();
  }

}
