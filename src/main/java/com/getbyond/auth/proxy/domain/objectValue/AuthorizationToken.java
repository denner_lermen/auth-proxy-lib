package com.getbyond.auth.proxy.domain.objectValue;


import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;

@Getter
@Setter
@EqualsAndHashCode(of = {"token"})
@ToString
@AllArgsConstructor
public class AuthorizationToken implements Serializable {

  private final String token;

}
