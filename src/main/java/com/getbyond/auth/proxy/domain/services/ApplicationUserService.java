package com.getbyond.auth.proxy.domain.services;

import com.getbyond.auth.proxy.domain.model.ApplicationUser;
import com.getbyond.auth.proxy.domain.objectValue.AuthorizationToken;
import java.util.Optional;

public interface ApplicationUserService {

  Optional<ApplicationUser> findBy(AuthorizationToken authorizationToken);
}
