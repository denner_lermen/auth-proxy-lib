package com.getbyond.auth.proxy.domain.model;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Company implements Serializable {

  private String id;
  private String name;
  private Long index;

}
