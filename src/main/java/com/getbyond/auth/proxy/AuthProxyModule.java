package com.getbyond.auth.proxy;

import java.util.TimeZone;
import javax.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackageClasses = {AuthProxyModule.class})
@EnableCaching
@EnableAutoConfiguration
@Slf4j
public class AuthProxyModule {

  public AuthProxyModule() {
    log.info("Auth proxy module loaded");
    TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
    log.info("Timezone set to UTC in application");
  }


  @PostConstruct
  void started() {
    TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
  }

}
