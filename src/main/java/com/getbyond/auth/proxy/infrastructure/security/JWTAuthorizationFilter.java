package com.getbyond.auth.proxy.infrastructure.security;


import com.getbyond.auth.proxy.domain.model.ApplicationUser;
import com.getbyond.auth.proxy.domain.model.Profile;
import com.getbyond.auth.proxy.domain.objectValue.AuthorizationToken;
import com.getbyond.auth.proxy.domain.services.ApplicationUserService;
import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

@Slf4j
public class JWTAuthorizationFilter extends BasicAuthenticationFilter {

  private ApplicationUserService applicationUserService;

  public JWTAuthorizationFilter(AuthenticationManager authManager, ApplicationUserService applicationUserService) {
    super(authManager);
    this.applicationUserService = applicationUserService;
  }

  @Override
  protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws IOException, ServletException {
    UsernamePasswordAuthenticationToken authentication = getAuthentication(req.getHeader(HttpHeaders.AUTHORIZATION));
    SecurityContextHolder.getContext().setAuthentication(authentication);
    chain.doFilter(req, res);
  }

  private UsernamePasswordAuthenticationToken getAuthentication(String token) {
    if (StringUtils.isBlank(token)) {
      ApplicationUser appUserNonAuthentication = new ApplicationUser();
      return new UsernamePasswordAuthenticationToken(appUserNonAuthentication, null, appUserNonAuthentication.getRoles());
    }

    AuthorizationToken authorizationToken = new AuthorizationToken(token);
    log.trace("Try authenticate user");
    ApplicationUser applicationUser = applicationUserService.findBy(authorizationToken).get();
    applicationUser.getRoles().add(Profile.nonAuthenticated());
    return new UsernamePasswordAuthenticationToken(applicationUser, null, applicationUser.getRoles());
  }
}
