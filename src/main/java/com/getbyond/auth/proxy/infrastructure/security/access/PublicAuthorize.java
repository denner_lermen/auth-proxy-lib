package com.getbyond.auth.proxy.infrastructure.security.access;


import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import org.springframework.security.access.prepost.PreAuthorize;

@Retention(RetentionPolicy.RUNTIME)
@PreAuthorize("hasAnyAuthority('PUBLIC','SUPER','ADMIN','USER')")
public @interface PublicAuthorize {
}
