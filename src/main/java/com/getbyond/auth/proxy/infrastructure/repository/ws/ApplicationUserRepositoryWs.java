package com.getbyond.auth.proxy.infrastructure.repository.ws;

import com.getbyond.auth.proxy.CacheName;
import com.getbyond.auth.proxy.domain.model.ApplicationUser;
import com.getbyond.auth.proxy.domain.objectValue.AuthorizationToken;
import com.getbyond.auth.proxy.domain.repository.ApplicationUserRepository;
import java.util.Optional;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

@Repository
@AllArgsConstructor
@Slf4j
public class ApplicationUserRepositoryWs implements ApplicationUserRepository {


  private HttpRequest httpRequest;


  @Override
  @Cacheable(value = CacheName.USER_AUTHENTICATED, key = "'user.token.' + #authorizationToken.token")
  public Optional<ApplicationUser> findMe(AuthorizationToken authorizationToken) {
    log.trace("Execute request to get authentication");
    ResponseEntity<ApplicationUser> response = httpRequest.get("auth/v1/users/me/auth", ApplicationUser.class, authorizationToken);
    return Optional.of(response.getBody());
  }
}
