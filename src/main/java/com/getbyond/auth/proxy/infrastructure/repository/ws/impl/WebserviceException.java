package com.getbyond.auth.proxy.infrastructure.repository.ws.impl;

public class WebserviceException extends RuntimeException {

  public WebserviceException() {
  }

  public WebserviceException(String s) {
    super(s);
  }

  public WebserviceException(String s, Throwable throwable) {
    super(s, throwable);
  }

  public WebserviceException(Throwable throwable) {
    super(throwable);
  }
}
