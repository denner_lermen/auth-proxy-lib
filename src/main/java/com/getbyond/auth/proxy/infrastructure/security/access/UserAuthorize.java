package com.getbyond.auth.proxy.infrastructure.security.access;


import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import org.springframework.security.access.prepost.PreAuthorize;

@Retention(RetentionPolicy.RUNTIME)
@PreAuthorize("hasAnyAuthority('USER','ADMIN','SUPER')")
public @interface UserAuthorize {
}

//@Retention(RetentionPolicy.RUNTIME)
//@PreAuthorize("#contact.name == authentication.name")
//public @interface ContactPermission {}
