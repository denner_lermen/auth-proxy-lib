package com.getbyond.auth.proxy.infrastructure.security;

import com.getbyond.auth.proxy.domain.model.ApplicationUser;
import com.getbyond.auth.proxy.domain.services.ApplicationUserService;
import java.util.Arrays;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.annotation.RequestScope;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@Configuration
@EnableWebSecurity
@AllArgsConstructor
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

  private ApplicationUserService userDetailsService;

  @Bean
  @RequestScope(proxyMode = ScopedProxyMode.NO)
  public ApplicationUser authenticatedUser() {
    return (ApplicationUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
  }


  @Override
  protected void configure(final HttpSecurity httpSecurity) throws Exception {
    httpSecurity.cors()
        .and().csrf().disable()
        .authorizeRequests().antMatchers(HttpMethod.GET, "/actuator", "/actuator/**").permitAll()
        .anyRequest().fullyAuthenticated().and()

        .addFilter(new JWTAuthenticationFilter(authenticationManager()))
        .addFilter(new JWTAuthorizationFilter(authenticationManager(), userDetailsService))

        .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
  }

  @Bean
  public CorsConfigurationSource corsConfigurationSource() {
    final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
    CorsConfiguration config = new CorsConfiguration().applyPermitDefaultValues();
    config.setAllowedMethods(Arrays.asList("GET", "PUT", "POST", "OPTIONS", "HEAD"));
    source.registerCorsConfiguration("/**", config);
    return source;
  }


}
