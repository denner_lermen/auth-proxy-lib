package com.getbyond.auth.proxy.infrastructure.repository.ws;

import com.getbyond.auth.proxy.domain.model.Company;
import com.getbyond.auth.proxy.domain.objectValue.CompanyId;
import com.getbyond.auth.proxy.domain.repository.CompanyRepository;
import java.util.Optional;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

@Repository
@AllArgsConstructor
public class CompanyRepositoryWs implements CompanyRepository {

  private HttpRequest httpRequest;

  @Override
  public Optional<Company> findById(CompanyId companyId) {
    ResponseEntity<Company> response = httpRequest.get("auth/v1/companies/" + companyId.getId(), Company.class);
    return Optional.of(response.getBody());
  }


}
