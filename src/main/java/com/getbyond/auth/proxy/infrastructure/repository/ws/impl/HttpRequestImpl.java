package com.getbyond.auth.proxy.infrastructure.repository.ws.impl;

import static java.util.Objects.isNull;

import com.getbyond.auth.proxy.domain.objectValue.AuthorizationToken;
import com.getbyond.auth.proxy.domain.services.CloudTokenService;
import com.getbyond.auth.proxy.infrastructure.repository.ws.HttpRequest;
import java.io.Serializable;
import javax.xml.ws.WebServiceException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
public class HttpRequestImpl implements HttpRequest {

  private String baseUrl;
  @Autowired
  private CloudTokenService cloudTokenService;

  public HttpRequestImpl(@Value("${byond.services.url}") String baseUrl) {
    this.baseUrl = baseUrl;
  }

  private SimpleClientHttpRequestFactory httpRequestFactory() {
    final SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
    factory.setReadTimeout(1000 * 15);
    factory.setConnectTimeout(1000 * 15);
    return factory;
  }

  private HttpEntity httpEntity(Serializable data, HttpHeaders headers) {
    if (isNull(data)) {
      return new HttpEntity(headers);
    }
    return new HttpEntity(data, headers);
  }

  private ResponseEntity execute(String url, HttpHeaders headers,
      HttpMethod method, Serializable data, Class responseType) {
    try {
      log.trace("Execute request to {} with method {}", url, method.getClass());
      final HttpEntity<String> entity = httpEntity(data, headers);
      SimpleClientHttpRequestFactory factory = httpRequestFactory();
      final RestTemplate restTemplate = new RestTemplate(factory);
      ResponseEntity response = restTemplate.exchange(url, method, entity, responseType);
      log.trace("Requested executed in url {} response status is {}", url, response.getStatusCode());
      isErrorResponse(url, response);
      return response;
    } catch (Exception e) {
      log.error("Fail on execute request {} in {}", method, url, e);
      throw new WebServiceException(e);
    }

  }

  private void isErrorResponse(String url, ResponseEntity responseEntity) {
    if (!responseEntity.getStatusCode().is2xxSuccessful()) {
      if (HttpStatus.NOT_FOUND.equals(responseEntity.getStatusCode())) {
        return;
      }
      throw new RuntimeException("Fail on execute request in URL: " + url + " Response Status: " + responseEntity.getStatusCode());
    }
  }

  public <T> ResponseEntity<T> get(String uri, Class<T> classTemp) {
    final HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    headers.set(HttpHeaders.AUTHORIZATION, cloudTokenService.getToken());
    return execute(getUrl(uri), headers, HttpMethod.GET, null, classTemp);
  }

  @Override
  public <T> ResponseEntity<T> get(String uri, Class<T> classTemp, AuthorizationToken authorizationToken) {
    final HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + authorizationToken.getToken());
    return execute(getUrl(uri), headers, HttpMethod.GET, null, classTemp);
  }

  @Override
  public <T> ResponseEntity<T> getUsingMasterAuthorization(String uri, Class<T> classTemp) {
    final HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    headers.set(HttpHeaders.AUTHORIZATION, cloudTokenService.getMasterToken());
    return execute(getUrl(uri), headers, HttpMethod.GET, null, classTemp);
  }

  private String getUrl(String uri) {
    if (uri.contains("https://") || uri.contains("http://")) {
      return uri;
    }
    return baseUrl + uri;
  }

  @Override
  public <T> ResponseEntity<T> postExternal(String url, Serializable data, Class<T> classTemp) {
    final HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    return execute(url, headers, HttpMethod.GET, data, classTemp);
  }

  @Override
  public <T> ResponseEntity<T> post(String url, Serializable data, Class responseType, AuthorizationToken authorizationToken) {
    final HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + authorizationToken.getToken());
    return execute(getUrl(url), headers, HttpMethod.POST, data, responseType);
  }

  @Override
  public <T> ResponseEntity<T> delete(String uri, Class responseType, AuthorizationToken authorizationToken) {
    final HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + authorizationToken.getToken());
    return execute(getUrl(uri), headers, HttpMethod.POST, null, responseType);
  }
}
