package com.getbyond.auth.proxy.infrastructure.repository.ws;

import com.getbyond.auth.proxy.domain.objectValue.AuthorizationToken;
import java.io.Serializable;
import org.springframework.http.ResponseEntity;

public interface HttpRequest {

  <T> ResponseEntity<T> get(String uri, Class<T> classTemp);

  <T> ResponseEntity<T> get(String uri, Class<T> classTemp, AuthorizationToken authorizationToken);

  <T> ResponseEntity<T> getUsingMasterAuthorization(String uri, Class<T> classTemp);

  <T> ResponseEntity<T> postExternal(String url, Serializable data, Class<T> classTemp);

  <T> ResponseEntity<T> post(String uri, Serializable data, Class responseType, AuthorizationToken authorizationToken);

  <T> ResponseEntity<T> delete(String uri,  Class responseType, AuthorizationToken authorizationToken);

}
